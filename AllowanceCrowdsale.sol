pragma solidity ^0.5.0;

import "./crowdsale/Crowdsale.sol";
import "./token/ERC20/IERC20.sol";
import "./token/ERC20/SafeERC20.sol";
import "./math/SafeMath.sol";
import "./math/Math.sol";


contract AllowanceCrowdsale is Crowdsale {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    address private _tokenWallet;


    constructor (address tokenWallet, uint256 rate, address payable wallet, IERC20 token) public Crowdsale(rate, wallet, token) {
        require(tokenWallet != address(0));
        _tokenWallet = tokenWallet;
    }

    
    function tokenWallet() public view returns (address) {
        return _tokenWallet;
    }

  
    function remainingTokens() public view returns (uint256) {
        return Math.min(token().balanceOf(_tokenWallet), token().allowance(_tokenWallet, address(this)));
    }

    function _deliverTokens(address beneficiary, uint256 tokenAmount) internal {
        token().safeTransferFrom(_tokenWallet, beneficiary, tokenAmount);
    }
}
